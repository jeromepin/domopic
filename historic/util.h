#pragma once

int initializeGPIO(const int PIN);

unsigned concatenate(unsigned x, unsigned y);

int stringToInteger(char *str);
