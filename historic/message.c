#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <wiringPi.h>

#include "message.h"
#include "util.h"

/* UNUSED FOR NOW
struct message {
	const char *synchronizationNibble = "1010",
	char *group[8],
	char *deviceId[8],
	char *state[8];
};
*/

char *createMessage(const char *group, const char *deviceId, const char *state) {
	const 	char *synchronizationNibble = "10101010";
	// Static is used to allocate memory on heap rather than stack : pointer still available after block's end
	static 	char message[29];
	sprintf(message, "%s%s%s%s", synchronizationNibble, group, deviceId, state);
	return message;
}

int sendMessage(const int PIN, char *message) {
	int size = (sizeof(message) / sizeof(message[0])) * 8;
	printf("\n%s (size : %d) on pin %d\n", message, size, PIN);

	for (int i = 0; i < size; i++) {
		if (message[i] == '1' || message[i] == '0') {
			printf("%c\n", message[i]);
			digitalWrite(6, message[i]);
			delay(1);
		}
	}
	
	delay(100);
	digitalWrite(PIN, 0);
	return 0;
}
