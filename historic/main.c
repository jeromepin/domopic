#include <stdio.h>

#include <wiringPi.h>

#include "util.h"
#include "message.h"

/*
 * 1010 	000000 		00000000 	00000000
 * sync 	 group 		    id 		hop + state
 *
 */

int main (int argc, char *argv[]) {
	const int PIN 	= 6;

	wiringPiSetup();
	pinMode(PIN, OUTPUT);

	char *message = NULL;

	if (argv[1] != NULL) {
		message = argv[1];
	} else {
		const char *group 	= "10001001";
		const char *deviceId 	= "10100010";
		const char *state 	= "10010101";
		message 		= createMessage(group, deviceId, state);
	}

	sendMessage(PIN, message);
	return 0;
}


