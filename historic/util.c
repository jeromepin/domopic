#include <stdio.h>
#include <stdlib.h>
#include <wiringPi.h>

#include "util.h"

int initializeGPIO(const int PIN) {
	
	if (wiringPiSetup() == -1)
		return 1;
 
	pinMode(PIN, OUTPUT);
	return 0;
}

unsigned concatenate(unsigned x, unsigned y) {
	unsigned pow = 10;
	while(y >= pow)
		pow *= 10;
	return x * pow + y;        
}

int stringToInteger(char *str) {
	return (int) strtol(str, NULL, 10);
}
