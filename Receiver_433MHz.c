/* RF Receiver using PIC16F887 microcontroller CCS C compiler code
   This RF receiver is based on rpi-rf default protocol
   Internal oscillator used @ 8MHz
*/

#include <16F887.h>
#fuses NOMCLR, NOBROWNOUT, NOLVP, INTRC_IO
#use delay(clock = 8MHz)
#use fast_io(D)
 
short code_ok = 0;
unsigned int8 nec_state = 0, i;
unsigned int32 rf_code = 0;

#INT_EXT                                            // External interrupt
void ext_isr(void){
  unsigned int16 time;
  if(nec_state != 0){
    time = get_timer1();                            // Store Timer1 value
    set_timer1(0);                                  // Reset Timer1
  }
  
  switch(nec_state){
    case 0 :                                        // Start receiving data (we're at the beginning of first bit)
      setup_timer_1( T1_INTERNAL | T1_DIV_BY_2 );   // Enable Timer1 module with internal clock source and prescaler = 2
      set_timer1(0);                                // Reset Timer1 value
      nec_state = 1;                                // Next state: end of first bit
      i = 0;
      ext_int_edge( H_TO_L );                       // Toggle external interrupt edge : High -> Low
      return;
    
    case 1 :                                        // End of 530µs or 1200µs space
       if((time > 1400) || (time < 400)){           // Invalid interval ==> stop decoding and reset
         nec_state = 0;                             // Reset decoding process
         setup_timer_1(T1_DISABLED);                // Disable Timer1 module
         return;
       }
       if( time > 900)                           // If pulse > 800µs (long pulse)
         bit_set(rf_code, (23 - i));             // Write 1 to bit (23 - i)
       else                                      // If pulse < 800µs (short space)
         bit_clear(rf_code, (23 - i));           // Write 0 to bit (23 - i)
       i++;
       if(i > 23){                               // If all bits are received
         code_ok = 1;                            // Decoding process OK
         disable_interrupts(INT_EXT);            // Disable the external interrupt
       }
       nec_state = 2;                            // Next state: end of width of 770µs or 1540µs
       ext_int_edge( L_TO_H );                   // Toggle external interrupt edge
       return;

    case 2 :                                        // End of width of 770µs or 1540µs
      if((time > 1400) || (time < 400)){            // Invalid interval ==> stop decoding and reset
        nec_state = 0;                              // Reset decoding process
        setup_timer_1(T1_DISABLED);                 // Disable Timer1 module
      }
      else
        nec_state = 1;                              // Next state: end of 500µs or 1000µs space
      ext_int_edge( H_TO_L );                     // Toggle external interrupt edge
      return;
  }
}

#INT_TIMER1                                      // Timer1 interrupt (used for time out)
void timer1_isr(void){
  nec_state = 0;                                 // Reset decoding process
  ext_int_edge( L_TO_H );                        // External interrupt edge from high to low
  setup_timer_1(T1_DISABLED);                    // Disable Timer1 module
  clear_interrupt(INT_TIMER1);                   // Clear Timer1 interrupt flag bit
}

void main() {
  setup_oscillator(OSC_8MHZ);                    // Set internal oscillator to 8MHz
  output_d(0);                                   // PORTD initial state
  set_tris_d(0);                                 // Configure PORTD pins as outputs
  enable_interrupts(GLOBAL);                     // Enable global interrupts
  enable_interrupts(INT_EXT_L2H);                // Enable external interrupt
  clear_interrupt(INT_TIMER1);                   // Clear Timer1 interrupt flag bit
  enable_interrupts(INT_TIMER1);                 // Enable Timer1 interrupt
  output_toggle(PIN_D0);                         // Enable RDO output
  while(TRUE){
    if(code_ok){                                 // If the mcu successfully receives 24 bits message
      code_ok = 0;                               // Reset decoding process
      nec_state = 0;
      setup_timer_1(T1_DISABLED);                // Disable Timer1 module
      if(rf_code == 0x00B8F47C)
          output_high(PIN_D1);
      if(rf_code == 0x00A98AC7)
        output_low(PIN_D1);
      enable_interrupts(INT_EXT_L2H);            // Enable external interrupt
    }
  }
}
